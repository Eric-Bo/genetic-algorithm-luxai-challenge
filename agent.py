import math, sys
from lux.game import Game
from lux.game_map import Cell, RESOURCE_TYPES
from lux.constants import Constants
from lux.game_constants import GAME_CONSTANTS
from lux import annotate
import pickle

DIRECTIONS = Constants.DIRECTIONS
game_state = None


def agent(observation, configuration):
    global game_state

    ### Do not edit ###
    if observation["step"] == 0:
        game_state = Game()
        game_state._initialize(observation["updates"])
        game_state._update(observation["updates"][2:])
        game_state.id = observation.player
    else:
        game_state._update(observation["updates"])
    
    actions = []

    ### AI Code goes down here! ### 
    player = game_state.players[observation.player]
    opponent = game_state.players[(observation.player + 1) % 2]
    width, height = game_state.map.width, game_state.map.height

    resource_tiles: list[Cell] = []
    for y in range(height):
        for x in range(width):
            cell = game_state.map.get_cell(x, y)
            if cell.has_resource():
                resource_tiles.append(cell)
    """
    #spam workers
    cities = player.cities
    for city in cities.values():
        for citytile in city.citytiles:
            if citytile.can_act():
                #print("build worker", file=sys.stderr)
                response = citytile.build_worker()

                #print(response, file=sys.stderr)
    """

    """
    # we iterate over all our units and do something with them
    for unit in player.units:
        if unit.is_worker() and unit.can_act():
            closest_dist = math.inf
            closest_resource_tile = None
            if unit.get_cargo_space_left() > 0:
                # if the unit is a worker and we have space in cargo, lets find the nearest resource tile and try to mine it
                for resource_tile in resource_tiles:
                    if resource_tile.resource.type == Constants.RESOURCE_TYPES.COAL and not player.researched_coal(): continue
                    if resource_tile.resource.type == Constants.RESOURCE_TYPES.URANIUM and not player.researched_uranium(): continue
                    dist = resource_tile.pos.distance_to(unit.pos)
                    if dist < closest_dist:
                        closest_dist = dist
                        closest_resource_tile = resource_tile
                if closest_resource_tile is not None:
                    actions.append(unit.move(unit.pos.direction_to(closest_resource_tile.pos)))
            else:
                # if unit is a worker and there is no cargo space left, and we have cities, lets return to them
                if len(player.cities) > 0:
                    closest_dist = math.inf
                    closest_city_tile = None
                    for k, city in player.cities.items():
                        for city_tile in city.citytiles:
                            dist = city_tile.pos.distance_to(unit.pos)
                            if dist < closest_dist:
                                closest_dist = dist
                                closest_city_tile = city_tile
                    if closest_city_tile is not None:
                        move_dir = unit.pos.direction_to(closest_city_tile.pos)
                        actions.append(unit.move(move_dir))
    """
    
        
    def translate_action(action_id, unit, player):
        """
        translate the number representation of an action into the actual action
        0: move north
        1: move east
        2: move south
        3: move west
        4: move to closest city
        5: pillage
        6: build city
        7: stay put
        """
        if action_id == 0:
            return unit.move(Constants.DIRECTIONS.NORTH)
        elif action_id == 1:
            return unit.move(Constants.DIRECTIONS.EAST)
        elif action_id == 2:
            return unit.move(Constants.DIRECTIONS.SOUTH)
        elif action_id == 3:
            return unit.move(Constants.DIRECTIONS.WEST)
        elif action_id == 4:
            if len(player.cities) > 0:
                    closest_dist = math.inf
                    closest_city_tile = None
                    for k, city in player.cities.items():
                        for city_tile in city.citytiles:
                            dist = city_tile.pos.distance_to(unit.pos)
                            if dist < closest_dist:
                                closest_dist = dist
                                closest_city_tile = city_tile
                    if closest_city_tile is not None:
                        move_dir = unit.pos.direction_to(closest_city_tile.pos)
                        return unit.move(move_dir)
        elif action_id == 5:
            return unit.pillage()
        elif action_id == 6:
            return unit.build_city()
        elif action_id == 7:
            return None
        else:
            raise Exception("invalid action_id")


    def get_situation(unit):
        """
        this function returns a dict specifying the current situation
        every agent has a genome of length 12500 = (5^5) * (2^2)
        with north, east, west, center, day/night, empty inven
        5 possibilities in order: 
            0: wall
            1: empty (includes uranium and coal for now)
            2: wood
            3: friendly city tile
            4: enemy city tile
        2 possibilities:
            0: day, 1:night
            0: full inventory, 1: space in inventory
        """
        current_pos = unit.pos
        situation = (get_tile_info("n", current_pos),
                     get_tile_info("e", current_pos),
                     get_tile_info("s", current_pos),
                     get_tile_info("w", current_pos),
                     get_tile_info("c", current_pos),
                     is_night(game_state.turn),
                     has_space_in_inventory(unit))

        return situation

    def get_tile_info(direction, pos):
        """
        returns the number corresponding to the content of the tile in the
        given direction.
        """
        target_tile_pos = [pos.x, pos.y]
        if direction == "n":
            target_tile_pos[1] + 1
        elif direction == "e":
            target_tile_pos[0] + 1
        elif direction == "s":
            target_tile_pos[1] - 1
        elif direction == "w":
            target_tile_pos[0] - 1
        elif direction == "c":
            pass
        else:
            raise Exception("invalid direction")

        # is wall? i.e. out of limits
        if target_tile_pos[0] < 0 or target_tile_pos[0] >= width or \
           target_tile_pos[1] < 0 or target_tile_pos[1] >= height:
           return 0

        # cell of interest
        cell = game_state.map.get_cell(target_tile_pos[0], target_tile_pos[1])

        # checking for wood
        if cell.resource == Constants.RESOURCE_TYPES.WOOD:
            return 2

        # checking for citytiles
        if cell.citytile is not None:
            if cell.citytile.team == player.team:
                return 3
            else:
                return 4

        # nothing was found, so there is nothing of interest on the tile
        return 1

    def is_night(turn):
        if turn % 40 > 30:
            return 1
        else:
            return 0

    def has_space_in_inventory(unit):
        if unit.get_cargo_space_left() == 0:
            return 0
        else:
            return 1

    # mapping situation -> action
    def get_action_id(genome, situation):
        """
        return the action specified by the genome for the given situation
        """
        return genome[situation]

    #get the genome
    with open("genome.pkl", "rb") as file:
        genome = pickle.load(file)

    for unit in player.units:
        if unit.can_act():
            situation = get_situation(unit)
            action_id = get_action_id(genome, situation)
            action = translate_action(action_id, unit, player)
            if action is not None:
                actions.append(action)

    def make_fitness(player):
        # the fitness is computed at the last turn:
        # 0.2*fuel in cities
        # 0.1*fuel in units
        # 100 points for each citytile
        # 20 points per unit
        #print(game_state.turn, file=sys.stderr)

        #the game end if there are no more cities or units      
        if game_state.turn == 360 or \
            (len(player.units) == 0 and \
            len(player.cities.values()) == 0):
            print("hmm", file=sys.stderr)
            fitness = 0
            fuel = 0
            n_citytiles = 0
            for city in player.cities.values():
                fuel += city.fuel
                n_citytiles += len(city.citytiles)
            fitness += fuel * 0.2
            fitness += n_citytiles * 10

            n_units = 0
            unit_fuel = 0
            for unit in player.units:
                unit_fuel += unit.cargo.wood
                n_units += 1
            fitness += n_units * 2
            fitness += unit_fuel * 0.1

            with open("fitness.pkl", "wb") as file:
                pickle.dump(fitness, file)      

    make_fitness(player)
    # you can add debug annotations using the functions in the annotate object
    # actions.append(annotate.circle(0, 0))
    
    return actions
