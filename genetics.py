import numpy as np
import pygad
import subprocess
import pickle

# every agent has a genome of length 12500 = (5^5) * (2^2)
# with north, east, west, center, day/night, empty inven
# 5 possibilities in order: 
    # 0: wall
    # 1: empty (includes uranium and coal for now)
    # 2: wood
    # 5: friendly city tile
    # 6: enemy city tile
# 2 possibilities:
    # 0: day, 1:night
    # 0: full inventory, 1: space in inventory
#genome = np.random.rand(5,5,5,5,5,2,2) * 7
#genome = np.ceil(genome)

def fitness_fun(solution, solution_idx):
    """
    calculate the fitness of each agent
    """
    #set the genome
    genome = Genome()
    genome.from_1D(solution)
    genome.save_genome()
    # execute the sim
    _ = subprocess.call("lux-ai-2021 main.py main.py --out=replay.json --loglevel=0", shell=True)
    #time.sleep(5)
    with open("fitness.pkl", "rb") as file:
        fitness = pickle.load(file)
    return fitness

def on_fitness(ga_instance, pop_fit):
    print("generations complete: " + str(ga_instance.generations_completed))
    print(pop_fit)
    with open("results.txt", "a") as f:
        f.write("generations complete: " + str(ga_instance.generations_completed) + "\n")
        f.write(str(pop_fit) + "\n")
        f.write("-----------------------" + "\n")

def main():
    ga_instance = pygad.GA(num_generations=30,
                           num_parents_mating=2,
                           gene_type=int,
                           gene_space=range(8),
                           fitness_func=fitness_fun,
                           sol_per_pop=100,
                           num_genes=12500,
                           init_range_low=0.0,
                           init_range_high=7.0,
                           mutation_percent_genes=0.01,
                           mutation_type="random",
                           mutation_by_replacement=True,
                           random_mutation_min_val=0.0,
                           random_mutation_max_val=7.0,
                           on_fitness=on_fitness)
    ga_instance.run()
    ga_instance.save("ga_instance.pkl")
    ga_instance.plot_fitness()

class Genome(object):
    def __init__(self):
        self.genome = np.zeros((5,5,5,5,5,2,2))

    def set_genome(self, new_genome):
        self.genome = new_genome

    def get_genome(self):
        return self.genome

    def get_idx(self):
        return np.ndarray.flatten(self.genome)

    def from_1D(self, solution):
        """
        since pygad only support 1D genomes, the genome needs to be reconstructed
        """
        idx = 0
        for i in range(5):
            for j in range(5):
                for k in range(5):
                    for l in range(5):
                        for m in range(5):
                            for n in range(2):
                                for o in range(2):
                                    self.genome[i,j,k,l,m,n,o] = solution[idx]
                                    idx += 1

    def save_genome(self):
        with open("genome.pkl", "wb") as file:
            pickle.dump(self.genome, file)


if __name__ == "__main__":
    main()